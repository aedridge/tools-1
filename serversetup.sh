#!/bin/sh

## Check amount of arguments
if [ "$#" -lt 1 ]
then
	echo 
	echo "Syntax:"
	echo "./serversetup.sh [-v] [-p php-version ] server-ip"
	echo
	echo "-v \t\t\tVerbose output"
	echo "-p PHP-VERSION\t\tSpecify the PHP version (5.3 [default] or 5.4)"
	echo
	echo "Example:"
	echo "./createrepo.sh -p 5.4 134.213.30.32"
	echo
	exit
fi

echo ""
echo "############################"
echo "##                        ##"
echo "## Automatic Server Setup ##"
echo "##                        ##"
echo "############################"
echo ""

##  Check which options have been applied and set variables accordingly

if [ "$#" -eq 9 ]
then
	fromapp=1
else
	fromapp=0
fi

if [ "$#" -eq 4 -o $fromapp -eq 1 ]
then
	echo "Verbose output & php version"
	
	phpver=$3
	serverip=$4
	output=/dev/stdout
	
else
	
	if [ "$#" -eq 2 ]
	then
		
		echo "Verbose output default PHP"
		
		phpver=5.3
		serverip=$2
		output=/dev/stdout
		
	else
		
		if [ "$#" -eq 3 ]
		then
			
			echo "PHP version specified + hidden output"

			phpver=$2
			serverip=$3
			output=/dev/null
		
		else # Only server IP specified
			
			echo "Default PHP + hidden output"
			
			phpver=5.3
			serverip=$1
			output=/dev/null
			
		fi
		
	fi
	
fi

## Check for a public key

if [ -f "$HOME/.ssh/id_dsa.pub" ]
then
	pub="$HOME/.ssh/id_dsa.pub"
else

	if [ -f "$HOME/.ssh/id_rsa.pub" ]
	then
		pub="$HOME/.ssh/id_rsa.pub"
	else
		echo | ssh-keygen -t rsa -N '' -f "$HOME/.ssh/id_rsa.pub"
		pub="$HOME/.ssh/id_rsa.pub"
	fi
	
fi

## Ensure correct permissions on key otherwise it will be ignored

chmod 700 "$pub"

publickey=$(cat "$pub")

## Add keys to keychain to prevent password prompt

{
	ssh-add -K
} &> "$output"

## If we're running from the app we don't need to take user input

if [ $fromapp -eq 1 ]
then
	
	websiteurl=$5
	newpassword=$6
	oldpassword=$7
	
else

	echo "Please enter the desired website URL (no http://)"
	read websiteurl
	echo ""
	
	if [ "${#websiteurl}" -eq 0 ]
	then
		echo ""
		echo "You need to enter a website URL"
		echo ""
		exit
	fi
	
	## Collect new root password
	
	echo "Please enter the new root password: "
	
	read -s newpassword
	
	if [ "${#newpassword}" -eq 0 ]
	then
		echo ""
		echo "You need to enter a root password"
		echo ""
		exit
	fi
	
	echo ""
	echo "Please confirm the password: "
	read -s confirmpass
	
	if [ "$newpassword" != "$confirmpass" ]
	then
		echo "Sorry these passwords do not match"
		exit
	fi
	
fi

echo ""
echo "Server IP:\t$serverip"
echo "PHP Version:\t$phpver"
echo "Website URL:\t$websiteurl"
echo "Website Path:\t/var/www/$websiteurl"
echo ""

## Install public key on server

echo "Connecting to server... Please enter the default root password: "
echo ""

if [ $fromapp -eq 1 ] ## If run from the app we need to use ssh pass
then
	
	export SSH_ASKPASS=\""$9"\"
	export SSHPASS="$oldpassword"
	
	echo \""$8"\"
	echo \""$9\""
	
	echo $publickey | \""$8"\" -e ssh -o StrictHostKeychecking=no "root@$serverip" 'mkdir /root/.ssh; touch /root/.ssh/authorized_keys; cat >> /root/.ssh/authorized_keys'
	
else ## We can need to prompt the user for input

	echo $publickey | ssh "root@$serverip" 'mkdir /root/.ssh; touch /root/.ssh/authorized_keys; cat >> /root/.ssh/authorized_keys'
	
fi

## Update root password to supplied one

echo ""
echo $newpassword | ssh -i "$pub" "root@$serverip" 'passwd --stdin root'
echo ""

## Download and execute the server configuration script on the remote server
{
	ssh -i "$pub" "root@$serverip" "yum -y install wget; wget -O /root/configserver.sh https://bitbucket.org/tapestry/tools/raw/master/configserver.sh; chmod 0777 /root/configserver.sh;"
} &> "$output"

ssh -i "$pub" "root@$serverip" "/root/configserver.sh $newpassword $websiteurl $phpver $output"